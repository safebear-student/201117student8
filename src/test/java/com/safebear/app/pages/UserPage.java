package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 20/11/2017.
 */
public class UserPage {
    WebDriver driver;
    @FindBy(linkText = "Logout")
    WebElement logoutLink;


    public UserPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Logged In");
    }

    public boolean clickOnLogout(WelcomePage welcomePage) {
        logoutLink.click();
        return welcomePage.checkCorrectPage();
    }
}