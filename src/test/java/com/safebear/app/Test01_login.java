package com.safebear.app;

import com.safebear.app.pages.UserPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 20/11/2017.
 */
public class Test01_login extends BaseTest {
    @Test
    public void testLogin(){
        //Step 1 Confirm we're on the welcome page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 Click on the Login link and the Login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
        //Step 4 Logout
        assertTrue(userPage.clickOnLogout(this.welcomePage));

    }

}
